package java8_simpler.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {

		List<String> names = new ArrayList<String>(Arrays.asList("Adam,Anna,Beatrice,Bert,Calle,Cecilia,Daniel,Diana,Erland,Ella,Fredrik,Freja".split(",")));

		// 1. Ett enkelt exempel p� vad lambda �r
		// interface:et Runnable  - f�rst utan lambda, sen med
		
		

		// "Lambda expressions" har formen 
		// (ett, eller, flera, argument) -> ettReturv�rde (eller Void, t.ex. System.out.println);
		// om det bara �r ett argument in, s� beh�ver vi inte ().
		
		
		
		
		// 2.0 Inbyggda anv�ndbara interface  (@FunctionalInterface) 

		// Se: https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html
		// ex. Function, Predicate, Supplier, Consumer, BiFunction
		// Dessa interface �r bra att anv�nda 
		// bland annat f�r att dom har en del anv�ndbara default-metoder (som man inte beh�ver implementera sj�lv)
		// och f�r att dom fungerar perfekt ihop med Streams (kommer senare).

		// 2.1 - Function<T,R>
		// En funktion/metod som tar ett argument av typ T och
		// returnerar ett v�rde av typ R. 
		// T.ex. en funktion som tar ett namn, och g�r om det till en h�lsningsfras + namn.
		
		
		System.out.println("\nH�lsa alla namn:");
		for (String name : names) {
			// System.out.println("");
		}
		
		
		
		// Precis som i en vanlig metod s� �r inparameterns namn helt godtyckligt (men.. kom ih�g clean code..)
		// Dom h�r funktionerna g�r samma sak:
		// T.ex. Function<Integer, Integer> add5 = number -> number + 5; 
		// T.ex. Function<Integer, Integer> spaceThingsUp = spaceMonkey -> spaceMonkey + 5; 
		
		
		
		
		// 2.2 - Predicate<T>
		// En funktion som tar ett argument av typ T och alltid returnerar en boolean
		// T.ex. en funktion som tar en str�ng och returnerar 
		// huruvida den �r l�ngre �n 5 chars eller inte.
		
		
		System.out.println("\nBara namn l�ngre �n 5 chars");
		for (String name : names) {
			if(false) { // ?
				System.out.println(name); // ?
			}
		}
		
		
		
		// 3. Stream<T> 
		// En stream �r en str�m av element (objekt) av typ T.
		// Den kan vara tom, o�ndligt l�ng, eller n�gonstans d�remellan.
		
		// List, Set, Map m.fl. har fr.o.m Java 8 metoden stream(), f�r att skapa en stream fr�n dom
		// T.ex. en Stream av namn (String), fr�n en lista (List<String) av namn
		Stream<String> nameStream = names.stream();
			
		// Men varf�r anv�nda Stream?
		// Stream erbjuder flera extremt anv�ndbara metoder f�r att f�r�ndra streamens inneh�ll
		// vi kanske vill att varje element i streamen ska f�r�ndras p� n�got s�tt,
		// att vissa element ska filtreras ut (eller, som kommer senare, grupperas och samlas upp) 
		
		// n�r vi har f�r�ndrat v�r stream till det vi vill ha
		// kan vi g�ra en sista operation med varje element via .forEach(Consumer<T>) 
		
		
		// T.ex. skriva ut alla element, var f�r sig, direkt via forEach
		System.out.println("\nAlla namn, fr�n en stream");
		
		
		
		
		
		// 4. .map(Function<T>)
		
		// PS. man kan bara anv�nda en stream en g�ng. s� varf�r ens spara den i en variabel?
		// man kan skapa streamen och direkt b�rja transformera den, t.ex. genom .map eller .filter
		
		// .map �r en metod som, via en Function<T,R>, "mappar" ett v�rde till ett annat.
		// .map f�rv�ntar sig allts� en Function<T,R> som argument.
		// Man kan se som att varje element i streamen 
		// kommer g� igenom funktionen och tranformeras enligt logiken i den
		
		// t.ex. g�ra om en stream av namn till en stream av namnens l�ngd
		System.out.println("\nAlla namns l�ngd (fr�n stream)");
		
		
		
		
		// 5. .filter(Predicate<T>)
		// .filter �r en metod som filtrerar ut (och beh�ller) alla element
		// som uppfyller ett visst krav (returnerar true)
		// .filter f�rv�ntar sig ett Predicate<T> som argument. 
		
		// t.ex. g�ra om en stream av namn till en stream av namn har bokstaven 'e' i sig
		System.out.println("\nAlla namn som inneh�ller bokstaven 'e' (fr�n stream)");
		
		
		// 6. Higher-order functions   +  "composability"
		
		// OBS! Higher-order functions!
		// map och filter �r "higher-order" funktioner, f�r 
		// att dom tar funktioner (Function, resp. Predicate) som argument.
		
		// En annan sorts "higher-order" funktion �r en funktion som returnerar en ny funktion.
		// se t.ex. metoden stringContains l�ngst ner.
		
		System.out.println("Kombinationen av tv� Predicate till ett,  och tv� Function till en: ");
		Predicate<String> containsE = stringContains("e");
		Predicate<String> containsA = stringContains("a");
		// TADA! ett nytt predikat som anv�nder sig av existerande logik i tv� andra predikat
		Predicate<String> containsEandA = containsE.and(containsA);
		
		Function<String,String> toUpperCase = str -> str.toUpperCase();
		Function<String,String> addExclamationMarks = str -> str + "!!!!!!!!!1";
		// TADA! en ny funktion, som fungerar precis som om funktionerna skulle k�ras efter varandra.
		Function<String,String> toAllCapsRage = toUpperCase.andThen(addExclamationMarks);
		
		System.out.println("Bara namn med 'e' OCH 'a' i sig, med stora bokst�ver och utropstecken: ");
		
		
		// 7. Specialversioner av Stream
		// en stream kan, via .map, g� fr�n en viss typ till en annan, som
		// vi redan sett (Stream<String> till Stream<Integer> ) 
		// men kan ocks� anv�nda special-versioner av .map s� att vi f�r
		// specialversioner av stream, som �r anpassade f�r en viss typ av data, t.ex. integer (IntStream)
		
		// om vi g�r det f�r vi till�ng till metoder som passar extra bra med den datatypen.
		// vi kan t.ex. f� ut summan av alla element i en IntStream, eller medelv�rde
		
		System.out.println("\nSumman av alla namns l�ngd = ");
		
		
		
		// 8. Collectors 
		// vi kan ocks� "collect":a en stream, s� att elementen in streamen samlas, t.ex. i en List, ett Set eller en Map
		
		// t.ex. g�ra en lista av alla namn med 'e' 
		
		System.out.println("\nNamn med e i sig, fr�n en stream vi collect:at till en List: ");
		
		
		
		// Collectors forts. - .groupingBy
		// om vi vill kan kan "collecta" en stream till en Map (en lista av key/value-par) och ange vad vi vill gruppera efter
		// t.ex. gruppera namn efter deras l�ngd.
		// 4=[Adam, Anna, Bert, Ella], 5=[Calle, Diana, Freja], 6=[Daniel, Erland], 7=[Cecilia, Fredrik], 8=[Beatrice]
		
		
		
		// en Map har ingen .stream() metod direkt p� sig, utan d�r beh�ver vi f�rst
		// f� ett "EntrySet" (ett Entry �r objekt som har en "key" och ett "value")
		System.out.println("\nNamn grupperade efter namnl�ngd: ");
		
		
		
		// Collectors forts. - .groupingBy
		// ist�llet f�r att g�ra en Collectors.toList kan vi g�ra andra operationer, t.ex. Collectors.counting.
		// t.ex. se hur "frekvensen" av namn med en viss l�ngd 
		// 4=4, 5=3, 6=2, 7=2, 8=1    (4 namn med 4 l�ngd 4, 5 med l�ngd 3, osv. )
		System.out.println("\nNamnfrekvens grupperat p� namnl�ngd: ");
		
		
		
		// Flatmap
		// ofta har man olika "custom" objekt som i sin tur har en samling objekt i sig, som man �r intresserad av
		// t.ex. en lista av users, d�r varje users har en lista av favoritmat. hur f�r man en samlad lista av allas favoritmat?
		// D� vill man "platta ut" objekt-strukturen, med 
		
		List<User> users = getAllUsers(); // 
		
		
		// vi skulle vilja ha en stream av favoritmat, som vi sen collectar till en List
		List<String> allFavoriteFoods;
		
		

	}

	
	// En enkel User-klass. Enda "funktionaliteten" �r att en user har en lista av sin favoritmat.
	static class User {
		public List<String> favoriteFoods;
		
		public User(String... favoriteFoods) {
			this.favoriteFoods = new ArrayList<String>(Arrays.asList(favoriteFoods));
		}
	}
	
	private static List<User> getAllUsers() {
		
		User u1 = new User("gurka","tomat","basilika", "paprika");
		User u2 = new User("kaka","pizza","s�s", "toast");
		User u3 = new User("gurkkaka","tomatpizza", "basilikas�s", "C-C-C-C-Combo Breaker");
		
		List<User> users = new ArrayList<>(Arrays.asList(u1,u2,u3));
		
		return users;
	}
	
	
	// Higher-order function   -  en metod/funktion som tar emot eller returnerar andra funktioner.
	// vi tar en str�ng A, och returnerar ett Predicate som testar ifall en (annan) str�ng B inneh�ller str�ng A
	private static Predicate<String> stringContains(String lettersToCheckFor) {
		Predicate<String> stringContainsProvidedLetters = str -> str.toLowerCase().contains(lettersToCheckFor);
		return stringContainsProvidedLetters;
		
		// OBS att vi ocks� kan returnera lambda-funktionen direkt,
		// eftersom vi (fr�n returtypen) vet vad lambdan representerar f�r sorts funktion.  (private static Predicate<String>...)
		// return str -> str.contains(lettersToCheckFor);
	}
}
