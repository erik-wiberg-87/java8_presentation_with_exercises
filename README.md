### En kort introduktion till några Java 8-features, med fokus på Stream api.

Repot skapades som underlag till en presentation på en 'meetup' för blivande Javautvecklare på Teknikhögskolan (Göteborg) och tar bland annat upp 
* lambda-notation
* Function 
* Predicate 
* Stream
* Collectors

[Till 'presentationen'](src/main/java/java8_simpler/main/Main.java) (Byt branch till 'solutions' för att se lösningarna)